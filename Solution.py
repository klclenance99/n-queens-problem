def get_board(n):
    result = []
    for num in range(n):
        result.append([0 for num in range(n)])
    return result

def is_safe(board, rowidx, columnidx):
    if 1 in board[rowidx]:
        return False
    for row in board:
        if 1 in row and row.index(1) == columnidx:
            return False
    for num in range(len(board)):
        for i in range(len(board)):
            if board[num][i] == 1 and (abs(i - columnidx) == abs(num - rowidx)):
                return False
    return True


# print(is_safe([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]], 3, 2))


def get_positions(n):
    board = get_board(n)
    start = 0
    i = 1
    while i < len(board):
        board[0][start] = 1
        for row in range(len(board)):
            for column in range(len(board)):
                if is_safe(board, row, column):
                    board[row][column] = 1
                    i += 1
        if i < len(board):
            board = get_board(n)
            start += 1
    return board


print(get_positions(4))
                
